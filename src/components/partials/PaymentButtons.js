import React from "react"
import styled from "styled-components"

const CoinPayments = props => {
  return (
    <Button>
      <form action="https://www.coinpayments.net/index.php" method="post">
        <input type="hidden" name="cmd" value="_pay" />
        <input type="hidden" name="reset" value="1" />
        <input
          type="hidden"
          name="merchant"
          value="12bc20b56e11e2af29d4b1f91639fb6b"
        />
        <input type="hidden" name="currency" value={props.currency} />
        <input type="hidden" name="amountf" value={props.amount} />
        <input type="hidden" name="item_name" value="Test Item" />
        <input
          type="image"
          src="https://www.coinpayments.net/images/pub/buynow-med.png"
          alt="Buy Now with CoinPayments.net"
        />
      </form>
    </Button>
  )
}

const Button = styled.div`
  form {
    margin: 0 5px;
  }
  form input {
    width: 100%;
    max-width: 150px;
    padding: 15px;
    border: 1px solid #e5e9eb;
    border-radius: 5px;
    box-sizing: border-box;
  }
`
const Paypal = props => (
  <Button>
    <form
      action="https://www.paypal.com/cgi-bin/webscr"
      method="post"
      target="_top"
    >
      <input type="hidden" name="cmd" value="_s-xclick" />
      <input type="hidden" name="hosted_button_id" value="6FZH6UVTHVJL4" />
      <input
        type="image"
        src="https://newsroom.mastercard.com/wp-content/uploads/2016/09/paypal-logo.png"
        border="0"
        name="submit"
        alt="PayPal - The safer, easier way to pay online!"
      />
      <img
        alt=""
        border="0"
        src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
        width="1"
        height="1"
      />
    </form>
  </Button>
)

export { CoinPayments, Paypal }
